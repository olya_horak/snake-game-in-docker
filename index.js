const express = require('express');
const app = express();
const port = 8082;

app.use(express.static(__dirname + '/public/'));

app.get('/', (req, res) => {
    res.sendFile('index.html')
});

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
});